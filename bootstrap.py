#!/usr/bin/env python
import os
from distutils.file_util import copy_file, DistutilsFileError
from distutils.dir_util import mkpath

def copy_tree(src, dst, ignores=()):
    if not os.path.isdir(src):
	raise DistutilsFileError, "cannot copy tree '%s': not a directory" % src

    try:
	names = os.listdir(src)
    except os.error, (errno, errstr):
	raise DistutilsFileError, "error listing files in '%s': %s" % (src, errstr)

    mkpath(dst)

    outputs = []

    for n in names:
	if n in ignores: continue

	src_name = os.path.join(src, n)
	dst_name = os.path.join(dst, n)

	if os.path.islink(src_name):
	    continue
	elif os.path.isdir(src_name):
	    outputs.extend(copy_tree(src_name, dst_name, ignores))
	else:
	    copy_file(src_name, dst_name, verbose=1)
	    outputs.append(dst_name)

    return outputs

from os.path import expanduser
home = expanduser("~")
#print home
ignore = ('.git','bootstrap.sh', 'bootstrap.py', 'README.md')
copy_tree('.',home,ignore)